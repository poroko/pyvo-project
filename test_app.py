import os
from app import index


def test_index_hello():
    result = index()

    assert "Hello" in result


def test_index_world():
    result = index()

    assert "World" in result


def test_env_var():
    assert os.environ.get("FLASK_ENV") == "testing"